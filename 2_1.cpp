#include <iostream>
#include <string>
using namespace std;

void mystery(string a, string& b){
    a.erase(0, 1);
    b += a[0];
    b.insert(3, "FOO");
}

int main() {
    string a = "BAR";
    string b = "FOO";
    mystery(a, b);
    cout << a << endl;
    cout << b << endl;
    return 0;
}