#include <iostream>

using namespace std;

const string DRINK_TYPE = "coffee";

void bottles(int count) {
    cout << count << " bottles of " << DRINK_TYPE << " on the wall" << endl;
    cout << count << " bottles of " << DRINK_TYPE << endl;
    cout << "Take one down, pass it around" << endl;
    cout << count - 1 << " bottles of " << DRINK_TYPE << " on the wall" << endl;
}

int main() {
    for(int i = 100; i > 0; i--){
        bottles(i);
    }
    return 0;
}